
const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

//User model 
// const User = require('./../models/User');

//User Controller
const userController = require('./../controllers/userControllers');

const auth = require('./../auth');

//syntax: router.HTTPmethod('uri', <request listener>)
// router.get('/', (req, res) => {
// 	// console.log("Hello from userRoutes")

// 	res.send("Hello from userRoutes")
// })

// http://localhost:3001/users
/*
router.post('/email-exists', (req, res) => {
	let email = req.body.email
	//find the matching document in the database using email
		//by using Model methods
	User.findOne({email: email}).then( (result, error) => {
		console.log(result)	//null

		if(result !== null){
			res.send(false)
		} else {

			if(result === null){
				//send back the response to the client
				res.send(true)
			} else {
				res.send(error)
			}
		}
	})
});
*/

/*Routes would only handle requests & responses*/

//check email route
router.post('/email-exists', (req, res) => {
	//invoke the function here
	userController.checkEmail(req.body.email).then(result => res.send(result))

});


//register route
router.post('/register', (req, res) => {
	//expecting data/object coming from request body

	userController.register(req.body).then(result => res.send(result))
})


router.get('/', (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
});


router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})


router.get('/details', auth.verify, (req, res) => {
	// console.log(req.headers.authorization)

	// console.log(auth.decode(req.headers.authorization))
	let userData = auth.decode(req.headers.authorization)

	userController.getUserProfile(userData).then(result => res.send(result))
})


//edit user information
router.put('/:userId/edit', (req, res) => {
	// console.log(req.params)
	// console.log(req.body)
	const userId = req.params.userId

	userController.editProfile(userId, req.body).then(result => res.send(result))
})

// http://localhost:3001/api/users/edit
router.put('/edit', auth.verify, (req, res) => {
	let userId = auth.decode(req.headers.authorization).id

	userController.editUser(userId, req.body).then(result => res.send(result))
})

//mini activity
// create a route to update user details with the following:
// endpoint = "/edit-profile"
// function name: editDetails
// method: put
// use email as filter to findOneAndUpdate() method

router.put("/edit-profile", (req, res) => {
	// console.log(req.body)

	userController.editDetails(req.body).then(result => res.send(result))
})


//mini activity
// create a route to delete a specific user with the following:
// endpoint = "/:userId/delete"
// function name: delete()
// method: delete
// use id as filter to findByIdAndDelete method
// return true if document was successfully deleted
router.delete("/:userId/delete", (req, res) => {
	// console.log(req.params.userId)

	userController.delete(req.params.userId).then(result => res.send(result))
})


//mini activityz
// create a route to delete a specific user with the following:
// endpoint = "/delete"
// function name: deleteUser() 
// method: delete
// use email as filter to findOneAndDelete method
// return true if document was successfully deleted
router.delete("/delete", (req, res) => {
	// console.log(req.headers.authorization)
	// console.log( auth.decode(req.headers.authorization).email)
	const email = auth.decode(req.headers.authorization).email

	userController.deleteUser(email).then(result => res.send(result))
})



module.exports = router;